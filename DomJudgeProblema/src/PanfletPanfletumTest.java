import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PanfletPanfletumTest {

	@Test
	void test() {
		//1
		String str1 = "TEST DE EMBARAZO";
		String str2 = "parnaso";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//2
		str1 = "BEBE";
		str2 = "bebe";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//3
		str1 = "GOLFO";
		str2 = "golfo";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//4
		str1 = "TEST DE EMBARAZO";
		str2 = "mamamio";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//5
		str1 = "PANFLETO";
		str2 = "qwertyuiop";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//6
		str1 = "BEBE";
		str2 = "tremendabullshit";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//7
		str1 = "PANFLETO";
		str2 = "paco";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//8
		str1 = "GOLFO";
		str2 = "poton";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		
		//1
		str1 = "PANFLETO";
		str2 = "aeiou";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//2
		str1 = "PANFLETO";
		str2 = "AAAAA";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//3
		str1 = "PANFLETO";
		str2 = " ";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//4
		str1 = "GOLFO";
		str2 = " o o";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//5
		str1 = "PANFLETO";
		str2 = "Amoga";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//6
		str1 = "PANFLETO";
		str2 = "";
		assertEquals(str1, PanfletPanfletum.problema(str2));
		//7
		str1 = "PANFLETO";
		str2 = "papapapepepepopo";
		assertEquals(str1, PanfletPanfletum.problema(str2));
	}
}
