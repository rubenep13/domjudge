import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class PanfletPanfletum {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int p = sc.nextInt();
		sc.nextLine();
		for (int i = 0; i < p; i++) {
			String nombre = sc.nextLine();
			System.out.println(problema(nombre));
		}
	}
	
	public static String problema (String nombre) {
		String res;
		int[] array = {0,0,0,0,0};
		for (int j = 0; j < nombre.length(); j++) {
			switch (nombre.charAt(j)) {
			case 'a':
				array[0]++;
				break;
			case 'e':
				array[1]++;
				break;
			case 'i':
				array[2]++;
				break;
			case 'o':
				array[3]++;
				break;
			case 'u':
				array[4]++;
				break;
			}
		}
		if(array[0] > array[1] && array[0] > array[3] && array[0] > array[2] && array[0] > array[4]) {
			res = "TEST DE EMBARAZO";
		}else if (array[1] > array[0] && array[1] > array[2] && array[1] > array[3] && array[1] > array[4]){
			res = "BEBE";
		}else if (array[3] > array[0] && array[3] > array[1] && array[3] > array[2] && array[3] > array[4]) {
			res = "GOLFO";
		}else {
			res = "PANFLETO";
		}
		return res;
	}
}